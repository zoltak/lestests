package exo;

public class PrixNegatifException extends Exception
{
	private double prixHT;
	
	public PrixNegatifException(double prixHT)
	{
		super("Prix négatif trouvé : " + prixHT);
	}

	public double getPrixHT()
	{
		return prixHT;
	}
	
}
