package lestests;

import org.junit.Assert;
import org.junit.Test;

public class MaPremiereClasseDeTest
{
	@Test
	public void abs_QuandNombrePositif_AlorsLeResultatEstLeNombre()
	{
		//Arrange
		double leNombre= 1234;
		
		//Act
		double resultat = Math.abs(leNombre);
		
		//Assert
		Assert.assertEquals(leNombre, resultat, 0 );
		//le 3ème paramètre représente le delta entre leNombre et resultat
	}
	
	@Test
	public void abs_QuandNombreNegatif_AlorsLeResultatEstMoinsLeNombre()
	{
		//Arrange
		double leNombre= -1234;
		
		//Act
		double resultat = Math.abs(leNombre);
		
		//Assert
		Assert.assertEquals(-leNombre, resultat, 0 );
		//le 3ème paramètre représente le delta entre leNombre et resultat
	}
	
	@Test
	public void abs_QuandNombreZero_AlorsLeResultatEstZero()
	{
		//Arrange
		double leNombre= 0;
		
		//Act
		double resultat = Math.abs(leNombre);
		
		//Assert
		Assert.assertEquals(0, resultat, 0 );
		//le 3ème paramètre représente le delta entre leNombre et resultat
	}
}