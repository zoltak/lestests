package exo;

import org.junit.Assert;
import org.junit.Test;

public class ArticleTest
{
	@Test
	public void calculTTC_QuandLePrixHorsTaxeEst1000_AlorsLePrixTTCEst1206() throws Exception
	{
		Article article = new Article("dummy reference", "dummy designation", 1000d);
		
		double prixTTC = article.calculTTC();
		
		Assert.assertEquals(1206, prixTTC, 0);
	}
	
	@Test
	public void setPrixHT_QuandLePrixEstNegatif_AlorsPrixNegatifException() throws Exception
	{
		Article article = new Article("dummy reference", "dummy designation");
		
		try
		{
			article.setPrixHT(-10d);
			Assert.fail("PrixNegatifException attendu");
		}
		catch (PrixNegatifException e)
		{
			//le setter setPrixHT de la classe Article est testé
			//le getter getPrixHT de la classe PrixNegatifException est testé
			Assert.assertEquals(-10d, e.getPrixHT(), 0);
		}
	}
	
}

